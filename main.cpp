#include <QCoreApplication>

#include <stdio.h>
#include <QtNetwork>
#include <QHostAddress>
#include <QTime>
#include <QDebug>

#include <messages_parsian_simurosot_data_wrapper.pb.h>


class MyThread : public QThread
{
protected:
    void run()
    {
        static const double minDuration = 0.1; //10FPS
        QUdpSocket* socket = new QUdpSocket();
        socket->bind(QHostAddress::AnyIPv4, 10030, QUdpSocket::ShareAddress);

        while(1) {
            DataWrapper packet;
            QByteArray datagram;
//            Logs* logs = packet.mutable_debugs();
//            Log* l = logs->add_msgs();
//            l->set_msg("Salam");
//            l->set_file("main.cpp");
//            l->set_level(LL_DEBUG);
//            l->set_function("main");
//            l->set_line("21");
//            Draws* draws = packet.mutable_draws();
//            Vec2D* v = draws->add_vectors();
//            v->set_x(1);
//            v->set_y(2);
//            Color* c = v->mutable_color();
//            c->set_a(1); c->set_b(1); c->set_g(1); c->set_r(1);
            WorldModel* wm = packet.mutable_worldmodel();
            wm->set_mode("asdf");

            MovingObject* bb = wm->mutable_ball();
            bb->set_id(0);
            Vector2D*p1 = bb->mutable_pos();
            Vector2D*p2 = bb->mutable_acc();
            Vector2D*p3 = bb->mutable_vel();
            p1->set_x(0); p1->set_y(0);
            p2->set_x(0); p2->set_y(0);
            p3->set_x(0); p3->set_y(0);
            wm->set_blue(false);
            wm->set_gamestate(PlayOn);
            Header* header = packet.mutable_header();
            header->set_seq(cnt++);
            header->set_stamp_second(QTime::currentTime().second());
            header->set_stamp_nsecond(QTime::currentTime().msec());
            loop_x++; loop_y++;
            if (loop_x > 90) loop_x -= 180;
            if (loop_y > 110) loop_y -= 220;
            Frame* detection = packet.mutable_detection();
            RBall* rb = detection->mutable_ball();
            rb->set_x(loop_x);
            rb->set_y(loop_y);
            RRobot* b = detection->add_robots_blue();
            RRobot* y = detection->add_robots_yellow();
            b->set_id(1); b->set_ang(0); b->set_x(-loop_x); b->set_y(loop_y);
            y->set_id(2); y->set_ang(90); y->set_x(-loop_x); y->set_y(-loop_y);
            datagram.resize(packet.ByteSize());
            bool success = packet.SerializeToArray(datagram.data(), datagram.size());
            if(!success) {
                qDebug() << "Serializing packet to array failed.";
            }
            quint64 bytes_sent = socket->writeDatagram(datagram, QHostAddress("224.5.23.2"), 10030);
            qDebug() << "sent = " << bytes_sent << ",   real = " << datagram.size();
            msleep(100);
            qDebug() << "Seq: " << packet.header().seq() << "RB: {" << rb->x() << ", " << rb->y() << "}";
        }
    }

public:
    int cnt, loop_x, loop_y;
    MyThread(QObject* parent = 0){cnt = 0; loop_x = loop_y = 0;}
    ~MyThread(){}
};

int main(int argc, char **argv)
{
    QCoreApplication a(argc, argv);
    MyThread thread;
    thread.start();
    thread.wait();
    return a.exec();
}
