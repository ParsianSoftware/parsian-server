QT += core network
QT -= gui

CONFIG += c++11

TARGET = parsian-server
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    proto/cpp/messages_parsian_simurosot_data_wrapper.pb.cc \
    proto/cpp/messages_parsian_simurosot_debugs.pb.cc \
    proto/cpp/messages_parsian_simurosot_detection.pb.cc \
    proto/cpp/messages_parsian_simurosot_worldmodel.pb.cc

HEADERS += \
    proto/cpp/messages_parsian_simurosot_data_wrapper.pb.h \
    proto/cpp/messages_parsian_simurosot_debugs.pb.h \
    proto/cpp/messages_parsian_simurosot_detection.pb.h \
    proto/cpp/messages_parsian_simurosot_worldmodel.pb.h

INCLUDEPATH+=proto/cpp
INCLUDEPATH+=/usr/local/Cellar/protobuf/3.6.0/include
LIBS+=-L/usr/local/Cellar/protobuf/3.6.0/lib/ -lprotobuf -lprotobuf-lite
